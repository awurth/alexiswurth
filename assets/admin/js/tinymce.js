import tinymce from 'tinymce/tinymce'

import 'tinymce/themes/silver'

import 'tinymce/plugins/advlist'
import 'tinymce/plugins/anchor'
import 'tinymce/plugins/autolink'
import 'tinymce/plugins/autoresize'
import 'tinymce/plugins/charmap'
import 'tinymce/plugins/code'
import 'tinymce/plugins/codesample'
import 'tinymce/plugins/emoticons'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/hr'
import 'tinymce/plugins/image'
import 'tinymce/plugins/insertdatetime'
import 'tinymce/plugins/link'
import 'tinymce/plugins/lists'
import 'tinymce/plugins/media'
import 'tinymce/plugins/nonbreaking'
import 'tinymce/plugins/paste'
import 'tinymce/plugins/preview'
import 'tinymce/plugins/print'
import 'tinymce/plugins/searchreplace'
import 'tinymce/plugins/tabfocus'
import 'tinymce/plugins/table'
import 'tinymce/plugins/textpattern'
import 'tinymce/plugins/toc'
import 'tinymce/plugins/visualblocks'
import 'tinymce/plugins/visualchars'
import 'tinymce/plugins/wordcount'

export default class {
  static apply (selector) {
    tinymce.init({
      selector: selector,
      plugins: this.getPlugins(),
      toolbar: this.getToolbar()
    })
  }

  static getPlugins () {
    return 'advlist anchor autolink autoresize charmap code codesample emoticons fullscreen hr image insertdatetime link lists media nonbreaking paste preview print searchreplace tabfocus table textpattern toc visualblocks visualchars wordcount'
  }

  static getToolbar () {
    return 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist checklist | forecolor backcolor | charmap emoticons | fullscreen preview print | image media link anchor codesample code'
  }
}
