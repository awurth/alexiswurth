import '../scss/main.scss'

import $ from 'jquery'
import TinyMCE from './tinymce'
import StackEdit from 'stackedit-js'

$(() => {
  TinyMCE.apply('textarea.tinymce')

  $('textarea.stackedit').each((index, textarea) => {
    const $textarea = $(textarea)
    /* const $button = $('<button>')
      .prop('type', 'button')
      .addClass('btn btn-secondary')
      .text('Edit with StackEdit')
      .css('margin-top', '1rem') */

    const stackedit = new StackEdit()

    // $button.click(() => {
    $textarea.click(() => {
      stackedit.openFile({
        content: {
          text: $textarea.val()
        }
      })
    })

    stackedit.on('fileChange', file => {
      $textarea.val(file.content.text)
    })

    // $textarea.after($button)
  })
})
