<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function findByProjectAndVersion(string $project, string $version, string $page)
    {
        return $this->createQueryBuilder('page')
            ->join('page.project', 'project')->addSelect('project')
            ->join('page.version', 'version')->addSelect('version')
            ->where('project.slug = :project')
            ->andWhere('version.name = :version')
            ->andWhere('page.slug = :page')
            ->setParameters([
                'project' => $project,
                'version' => $version,
                'page' => $page
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
