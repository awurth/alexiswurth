<?php

namespace App\Admin\Menu;

use App\Admin\PageAdmin;
use App\Admin\ProjectAdmin;
use App\Repository\ProjectRepository;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\Provider\MenuProviderInterface;
use Sonata\AdminBundle\Admin\Pool;

class MenuProvider implements MenuProviderInterface
{
    protected $factory;

    protected $pool;

    protected $projectRepository;

    public function __construct(FactoryInterface $factory, Pool $pool, ProjectRepository $projectRepository)
    {
        $this->factory = $factory;
        $this->pool = $pool;
        $this->projectRepository = $projectRepository;
    }

    public function get($name, array $options = []): ?ItemInterface
    {
        if ('admin_menu_provider' !== $name) {
            return null;
        }

        $menu = $this->factory->createItem('admin.group.'.$options['name'], [
        ]);

        $menu->setAttribute('class', 'keep-open');
        $menu->setExtra('keep_open', true);

        $projects = $this->projectRepository->findBy([], ['position' => 'ASC']);

        if (count($projects)) {
            foreach ($projects as $project) {
                $menu->addChild($this->getMenuItem($project->getName(), ProjectAdmin::class, PageAdmin::class.'.list', ['id' => $project->getId()]));
            }
        } else {
            $menu->addChild($this->getMenuItem('Add project', ProjectAdmin::class, 'create'));
        }

        return $menu;
    }

    public function has($name, array $options = []): bool
    {
        return 'admin_menu_provider' === $name;
    }

    protected function getMenuItem(string $label, string $adminCode, string $route, array $parameters = []): ItemInterface
    {
        $admin = $this->pool->getInstance($adminCode);

        $options = $admin->generateMenuUrl($route, $parameters);
        $options['extras'] = [
            'label_catalogue' => $admin->getTranslationDomain(),
            'admin' => $admin,
        ];
        return $this->factory->createItem($label, $options);
    }
}