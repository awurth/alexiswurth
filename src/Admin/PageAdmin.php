<?php

declare(strict_types=1);

namespace App\Admin;

use App\Repository\VersionRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

final class PageAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'page';

    protected function configureBatchActions($actions): array
    {
        return [];
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('version', null, [
                'query_builder' => function (VersionRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.project = :project')
                        ->setParameter('project', $this->getRequest()->get('id'))
                        ->orderBy('v.id', 'DESC');
                }
            ])
            ->add('title')
            ->add('slug')
            ->add('content', null, [
                'attr' => [
                    'class' => 'stackedit',
                    'rows' => 10
                ],
                'required' => false
            ]);
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id', null, [
                'header_class' => 'text-center',
                'row_align' => 'center'
            ])
            ->addIdentifier('title')
            ->addIdentifier('position', null, [
                'header_class' => 'text-center',
                'row_align' => 'center'
            ]);
    }

    protected function configureRoutes(RouteCollection $collection): void
    {
        if (!$this->isChild()) {
            $collection->clear();
            return;
        }

        $collection->remove('export');
    }
}
