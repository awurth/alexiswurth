<?php

/** @noinspection PhpDocSignatureInspection */

namespace App\Controller;

use App\Repository\PageRepository;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DocumentationController extends AbstractController
{
    /**
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     * @var ProjectRepository
     */
    protected $projectRepository;

    public function __construct(PageRepository $pageRepository, ProjectRepository $projectRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @Route("/projects/{projectSlug}/v{versionName}/{pageSlug}", name="app_doc_home")
     */
    public function documentation(string $projectSlug, string $versionName, string $pageSlug): Response
    {
        if (!$page = $this->pageRepository->findByProjectAndVersion($projectSlug, $versionName, $pageSlug)) {
            throw $this->createNotFoundException();
        }

        $projects = $this->projectRepository->findAll();

        return $this->render('front/documentation/index.html.twig', [
            'page' => $page,
            'projects' => $projects
        ]);
    }
}
