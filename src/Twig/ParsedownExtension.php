<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ParsedownExtension extends AbstractExtension
{
    protected $parsedown;

    public function __construct(\ParsedownExtra $parsedown)
    {
        $this->parsedown = $parsedown;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('markdown', [$this->parsedown, 'text'], ['is_safe' => ['html']])
        ];
    }
}
